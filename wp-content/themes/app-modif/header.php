<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package app-modif
 */

?>
<!doctype html>
<html lang="ru"> 
<head>
<meta charset="utf-8">
<title>Notarius</title>
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.5, user-scalable=yes"/>
<link rel="shortcut icon" href="#" type="image/x-icon">
<style>body{overflow-x:hidden;}.preloader{position:fixed;top:0;left:0;bottom:0;right:0;z-index:10000;background-color:#3F70A9;}</style>
<?php wp_head(); ?>
</head>

<body class="home">
<div class="preloader"></div>

<div id="page" class="site">

	<header class="header bg--cover" style="background-image:url(<?php echo get_template_directory_uri() . '/img/header/bg-top.jpg' ?>)">
		<div class="overlay"></div>
		<div class="top-line">
			<div class="container">
				<div class="row">
					<div class="mobil-menu">
						<a href="#my-menu" class="hamburger hamburger--emphating">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</a>
						<nav id="my-menu">
							<ul class="menu_top">
								<li><a href="#address">Адрес</a></li>
								<li><a href="#map">Карта</a></li>
								<li><a href="#map">Схема</a></li>
								<li><a href="#hours">Рабочие часы</a></li>
								<li><a href="#contacts">Почта</a></li>
								<li><a href="#contacts">Телефон</a></li>
							</ul>
						</nav>
					</div>
					<div class="menu-wrap">
						<nav class="decstop-menu">
							<ul class="menu_top">
								<li><a href="#address">Адрес</a></li>
								<li><a href="#map">Карта</a></li>
								<li><a href="#map">Схема</a></li>
								<li><a href="#hours">Рабочие часы</a></li>
								<li><a href="#contacts">Почта</a></li>
								<li><a href="#contacts">Телефон</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="head-title">
			<div class="container">
				<div class="row">
					<h1>Нотариус</h1>
					<h2 class="top-subtitle">Бублий Д. С.</h2>
					<p class="oficial">Официальный сайт. Легкая версия</p>
				</div>
			</div>
		</div>
		<div class="bottom-line">
			<div class="container">
				<div class="row">
					<div class="bottom-content">
						<div class="col--25">
							<h2 class="search"><i class="fa fa-search" aria-hidden="true"></i>Поиск</h2>
						</div>
						<div class="col--50">
							<div class="field-wrap">
								<input class="search-field" placeholder="Введите слово или фразу">
							</div>
						</div>
						<div class="col--25">
							<div class="button-wrap">
								<a class="button-search">Найти</a>
							</div>
						</div>
					</div>
					<div style="clear: both"></div>
				</div>
			</div>
		</div>
	</header>

	<div id="content" class="site-content">

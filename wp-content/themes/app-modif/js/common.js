$(document).ready(function() {
	
	// SELECT
	
	$('select').selectize({create: true});
	
	$( ".reset-wrap .reset-button" ).click(function() {
		$(".select")[0].reset();
	});
	
	
	// MMENU 
	
	$('#my-menu').mmenu({
		extensions:['theme-black', 'effect-menu-slide', 'pagedim-black'],
		offCanvas:{
			position: 'left'
		},
		navbar:{
			title:'<div class="slide-menu"></div>'
		}
	});
	
	var api = $('#my-menu').data('mmenu');
	api.bind('close:start', function(){
		$('.hamburger').addClass('is-active');
	});
	api.bind('open:before', function(){
		$('.hamburger').removeClass('is-active');
	});
	
	// SCROLLING
	
	var lastId,
		topMenu = $(".menu_top"),
		topMenuHeight = topMenu.outerHeight()+15,
		menuItems = topMenu.find("a"),
		scrollItems = menuItems.map(function(){
		  var item = $($(this).attr("href"));
		  if (item.length) { return item; }
		});

	menuItems.click(function(e){
	  var href = $(this).attr("href"),
		  offsetTop = href === "#" ? 0 : $(href).offset().top;
	  $('html, body').stop().animate({ 
		  scrollTop: offsetTop
	  }, 1000);
	  e.preventDefault();
	});
	
	// CARD REPLACEMENT
	
	$( ".scheme-button.mapping" ).click(function() {
		$( ".map-responsive" ).fadeOut( "slow", function() {
			$( ".scheme-button.mapping" ).fadeOut(0);
			$( ".scheme-button.travel" ).fadeIn(0);
		});
		$( ".map-static" ).fadeIn( "slow", function() {});
	});
	
	
	$( ".scheme-button.travel" ).click(function() {
		$( ".map-static" ).fadeOut( "slow", function() {
			$( ".scheme-button.mapping" ).fadeIn(0);
			$( ".scheme-button.travel" ).fadeOut(0);
		});
		$( ".map-responsive" ).fadeIn( "slow", function() {});
	});
	
	
 
});


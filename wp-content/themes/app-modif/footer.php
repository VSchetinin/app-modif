<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package app-modif
 */

?>

	</div><!-- #content -->
	
	
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col--33">
					<div class="content-left">
						<p>Все права защищены</p>
						<p><a href="#" target="_blank" title="Политика конфиденциальности">Политика конфиденциальности</a></p>
					</div>
				</div>
				<div class="col--33">
					<div class="content-center">
						<p>Полная версия официального</p>
						<p><a href="#" target="_blank" class="und">сайта нотариуса</a></p>
					</div>
				</div>
				<div class="col--33">
					<div class="content-right">
						<p>Сайт разработан компанией <a href="https://ayeps.ru/" target="_blank">AYEP'S</a></p>
						<p>Использованы модули <a href="#">InotaryWeb</a></p>
					</div>
				</div>
			</div>
		</div>
		<div style="clear: both"></div>
	</footer>
	

<!-- Preloader Script -->
<script>$(window).on('load', function() {$('.preloader').delay(500).fadeOut(500);});</script>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

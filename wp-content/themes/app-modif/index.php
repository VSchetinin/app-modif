<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package app-modif
 */

get_header(); ?>


	<main class="main bg--cover" style="background-image:url(<?php echo get_template_directory_uri() . '/img/main/main-fon.jpg' ?>)">
		
		<section class="s-assistant" id="assistant">
			<h2>Интерактивный помощник</h2>
			<p>Выберите ключевые слова</p>
			<div class="select-wrapper">
				<div class="container">
					<div class="row">
					<div class="form-wrap">
						<form class="select">
							<div class="form-body">
								<div class="col--33">
									<select name="key-1">
										<option value disabled selected></option>
										<option>Key 1</option>
										<option>Key 2</option>
										<option>Key 3</option>
										<option>Key 4</option>
									</select>
								</div>
								<div class="col--33">
									<select name="key-2">
										<option value disabled selected></option>
										<option>Key 1</option>
										<option>Key 2</option>
										<option>Key 3</option>
										<option>Key 4</option>
									</select>
								</div>
								<div class="col--33">
									<select name="key-3">
										<option value disabled selected></option>
										<option>Key 1</option>
										<option>Key 2</option>
										<option>Key 3</option>
										<option>Key 4</option>
									</select>
								</div>
								<div style="clear: both"></div>
							</div>
						</form>
					</div>
						<div class="reset-wrap">
							<button class="reset-button">Сброс</button>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="s-address bg--cover" id="address" style="background-image:url(<?php echo get_template_directory_uri() . '/img/main/main-fon-1.jpg' ?>)">
			<div class="content-wrap">
				<div class="container">
					<div class="row">
						<h2><i class="fa fa-map-marker" aria-hidden="true"></i>Адрес: </h2>
						<p>г. Москва, ул.Студенческая, д.44/28</p>
					</div>
				</div>
			</div>
		</section>
		
		<section class="s-map" id="map">
			<div class="container">
				<div class="row">
					<h2>Карта</h2>
					<div class="scheme-wrap">
						<button class="scheme-button mapping">Схема проезда</button>
						<button class="scheme-button travel">Показать карту</button>
					</div>
					<div class="map-wrapper">
						<div class="map-responsive">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2246.126444182276!2d37.53573231598223!3d55.73893198054961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54be927929b9d%3A0xaefa8c507ff99233!2z0KHRgtGD0LTQtdC90YfQtdGB0LrQsNGPINGD0LsuLCA0NC8yOCwg0JzQvtGB0LrQstCwLCAxMjExNjU!5e0!3m2!1sru!2sru!4v1509588087898" width="1170" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>						
						</div>
						<div class="map-static">
							<img class="schema img-responsive" src="<?php echo get_template_directory_uri() . '/img/main/www.jpg' ?>" alt="schema">
						</div>
					</div>
					<div style="clear: both"></div>
				</div>
			</div>
		</section>
		
		<section class="s-hours bg--cover" id="hours" style="background-image:url(<?php echo get_template_directory_uri() . '/img/main/main-fon-2.jpg' ?>)">
			<div class="content-wrap">
				<div class="container">
					<div class="row">
						<div class="col--50 left">
							<div class="work-time">
								<img src="img/main/watch.png" class="watch" alt="" >
								<h2>Рабочие часы: </h2>
							</div>	
						</div>
						<div class="col--50 right">
							<div class="button-wrap" style="position:relative">
								<a href="#" class="hours-button">Посмотреть актуальное расписание</a>
							</div>
						</div>
						<div style="clear: both"></div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="s-contact-title">
			<h2>Контакты</h2>
		</section>
		
		<section class="s-contacts bg--cover" id="contacts" style="background-image:url(<?php echo get_template_directory_uri() . '/img/main/main-fon-3.jpg' ?>)">
			<div class="container">
				<div class="row">
					<div class="col--50 left">
						<div class="contact-wrap contact-mail">
							<h2><i class="fa fa-envelope" aria-hidden="true"></i>Почта</h2>
							<p class="upp"><a href="mailto:notarius@inotary.ru">notarius@inotary.ru;</a></p>
						</div>	
					</div>
					<div class="col--50 line">
						<div class="contact-wrap contact-phone">
							<h2><i class="fa fa-phone" aria-hidden="true"></i>Телефон</h2>
							<p>Viber:<a href="viber://chat?number=7000000000"> +7000 000 000</a></p>
							<p>WhatsApp:<a href="#"> +7000 000 000</a></p>
							<p>Телефон:<a href="tel:+74992494645"> +7 499 249 46 45</a></p>
						</div>
					</div>
				</div>
			</div>
			<div style="clear: both"></div>
		</section>
		
	</main>

<?php
get_footer();
